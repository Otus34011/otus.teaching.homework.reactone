import './Input.css'

type Props = {
    id: string;
    placeholder: string;
};

function Input({ id, placeholder }: Props) {
    return (
        <input
            id={id}
            placeholder={placeholder}
        />
    );
}

export default Input;