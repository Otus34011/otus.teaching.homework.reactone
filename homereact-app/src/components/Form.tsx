import { Component } from "react";
import axios from 'axios';
import './Form.css'
import Input from "./Input";
import Result from "./Result";

interface Response {
    status: number;
    message: string;
}

class Form extends Component<{}, Response> {
    constructor(props: {}) {
      super(props);
      this.state = { status: 200, message: 'Response place' };
    }

    sendRequest = async () => {
            
        var urlInput = document.getElementById('inputUrl');
      
        if (!urlInput) {
            this.setState({ status: 500, message: 'Component not found' });     
            return;
        }
  
        var url = (urlInput as HTMLInputElement).value;
        if(url === ''){
            this.setState({ status: 400, message: 'Url is empty'});     
            return;
        }

        var jsonResponse;
        try{
            jsonResponse = await axios.get(url);
            const response = JSON.stringify(jsonResponse.data, undefined, 2); 
            console.log(response);
            this.setState({ status: jsonResponse.status, message: response });            
        }
        catch(e){
            console.error(e);
            this.setState({ status: 400, message: 'Bad request'});            
        }
    }     
  
    render() {
        return (
            <div>
                <div style={{display:"flex"}}>
                    <Input id="inputUrl" placeholder="Enter URL"/>
                    <button onClick={this.sendRequest}>Send</button>
                </div>
                <Result status={this.state.status} text={this.state.message}/>
            </div>            
        );
    }   
}  

export default Form;