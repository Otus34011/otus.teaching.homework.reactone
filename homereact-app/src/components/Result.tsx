import './Result.css'

type Props = {
    status: number;
    text: string;
};

function Result({ status, text }: Props) {
    var resultStyle = status === 200 ? {color : 'black'} : {color : 'red'};
    return (
        <div style={resultStyle}>
            <pre>
                {text}
            </pre>
        </div>
    );
}

export default Result;